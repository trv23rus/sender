<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use GuzzleHttp\Client;
//use GuzzleHttp\Psr7\Request as PsrRequest;

class SendController extends Controller
{
    

    public function __construct()
    {
      	//
    }

    
    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * 
     */
    public function saveAndSend(Request $request)
    {
       
        
        $this->validate($request, [
            'remote-url'    => 'required',
            'sum'           => 'required',
            'commission'    => 'required',
            'order-number'  => 'required',
          ]);
        
        try {
            $package = [];
            $this->save($request, $package);
            $send = $this->send($request, $package);

            
            if ($request->ajax() && $send == 200) 
            {
                
                    return response()->json(["success" => "true",]);
            }

            return true;
        }
        
        catch (GeneralException $e) {
            if ($request->ajax()) {
                return response()->json($e->getMessage(), 400);
            }
        }
        
    }

    private function save($request, &$package)
    {
        $sum            = $request->input('sum');
        $commission     = $request->input('commission');
        $orderNumber    = $request->input('order-number');
        $error = false; 
        for ($i=0; $i<count($sum); $i++)
        {
            $payment = new Payment();
            $payment->sum           = $sum[$i];
            $payment->commission    = round($commission[$i], 2);
            $payment->order_number  = $orderNumber[$i];
            if (!$payment->save()) $error = true;
            else {
                $tempArr = array($payment->id, $payment->sum, $payment->commission, $payment->order_number);
                array_push($package, $tempArr);
            }
            
        }
        if ($error) return false;
        else return true;

    }

    private function send($request, $package)
    {
        $baseUri = $request->input('remote-url');
        $client = new Client(['base_uri' => $baseUri]);
        $headers = ['API-SECRET' => getenv('SECRET')];
        
        $res = $client->request('POST', '/api/store', ['headers' => $headers, 'query' => $package]);
        $encode = json_decode($res->getStatusCode(), true);
        
        return $encode;
        
        
    }

}
